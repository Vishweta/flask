from flask import Flask, jsonify, request, abort, session, flash,redirect,url_for
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_cors import CORS, cross_origin
import psycopg2
import psycopg2.extras
from datetime import timedelta


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:postgres@localhost/user'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['SECRET_KEY'] = 'cairocoders-ednalan'  
app.config['PERMANENT_SESSION_LIFETIME'] =  timedelta(minutes=10)

CORS(app)

DB_HOST = "localhost"
DB_NAME = "user"
DB_USER = "postgres"
DB_PASS = "postgres"

conn = psycopg2.connect(dbname = DB_NAME, user = DB_USER, password = DB_PASS, host = DB_HOST,) 


db = SQLAlchemy(app)



class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key = True)
    user_name = db. Column(db.String(100), nullable = False)
    user_phone = db.Column(db.Integer, nullable = False)
    user_email = db.Column(db.String(100), nullable = False)
    user_address = db.Column(db.String(100), nullable = False)
    user_password = db.Column(db.String(100), nullable = False)


    def __repr__(self):
        return "<user %r>" % self.user_name


@app.route('/')
def home():
    passhash = generate_password_hash('cairocoders')
    print(passhash)
    if 'username' in session:
        username = session['username']
        return jsonify({'message' : 'You are already logged in', 'username' : username})
    else:
        resp = jsonify({'message' : 'Unauthorized'})
        resp.status_code = 401
        return resp

@cross_origin()
@app.route('/signup', methods = ['GET','POST'])
def signup_user():
    
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

    if request.method == 'POST' and 'user_name' in request.form and 'user_password' in request.form and 'user_email' in request.form:
    
        user_data = request.json                                                           
        user_name = user_data['user_name']
        user_email = user_data['user_email']
        user_password= user_data['user_password']

        _hashed_password = generate_password_hash(user_password)

        cursor.execute('SELECT * FROM users WHERE username = %s', (user_name,))
        account = cursor.fetchone()
        print(account)

        if account:
            flash('Account already exists!')
        elif not re.match(r'[^@]+@[^@]+\.[^@]+',user_email):
            flash('Invalid email address!')
        elif not re.match(r'[A-Za-z0-9]+', user_name):
            flash('Username must contain only characters and numbers!')
        elif not user_name or not  user_password or not user_email:
            flash('Please fill out the form!')
        else:
            cursor.execute("INSERT INTO users (fullname, username, password, email) VALUES (%s,%s,%s,%s)", (user_name, _hashed_password, user_email))
            conn.commit()
            flash('You have successfully registered!')

    elif request.method == 'POST':
        
        flash('Please fill out the form!')
  

    return jsonify({"success": True,"response":"user signup"})



@app.route('/login', methods=['GET','POST'])
def login():
    msg = ''
    if request.method == 'POST' and 'user_name' in request.form and 'user_password' in request.form:
        user_name = request.form['user_name']
        user_password = request.form['user_password']
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute('SELECT * FROM users WHERE username = %s', (user_name,))

        account = cursor.fetchone()
        if account:
            session['loggedin'] = True
            session['id'] = account['id']
            session['user_name'] = account['user_name']
            msg = 'Logged in successfully !'
        
        else:
            msg = 'Incorrect username / password !'
    
        if row:
            if check_password_hash(user_password, user_password):
                session['user_name'] = user_name
                cursor.close()
                return jsonify({'message' : 'You are logged in successfully'})
            else:
                resp = jsonify({'message' : 'Bad Request - invalid password'})
                resp.status_code = 400
                return resp
    else:
        resp = jsonify({'message' : 'Bad Request - invalid credendtials'})
        resp.status_code = 400
        return resp


@app.route('/logout',methods = ['GET','POST'])
def logout():

   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('username', None)
   return redirect(url_for('login'))



@cross_origin()
@app.route('/addusers', methods = ['GET','POST'])
def create_user():
    user_data = request.json

    user_name = user_data['user_name']
    user_phone = user_data['user_phone']
    user_email = user_data['user_email']
    user_address = user_data['user_address']
    user_password=user_password['user_password']


    user = Users(user_name=user_name, user_phone = user_phone,user_email = user_email,user_address = user_address,user_password=user_password)
    db.session.add(user)
    db.session.commit()
    

    return jsonify({"success": True,"response":"user added"})


@cross_origin()    
@app.route('/getuser', methods = ['GET'])
def getusers():
     all_users = []
     users = Users.query.all()
     for user in users:
          results = {
                    "user_id":user.id,
                    "user_name":user.user_name,
                    "user_phone":user.user_phone,
                    "user_email":user.user_email,
                    "user_address":user.user_address, }
          all_users.append(results)

     return jsonify(
            {
                "success": True,
                "users": all_users,
                "total_users": len(users),
            }
        )

@cross_origin()  
@app.route("/users/<int:user_id>", methods = ["PATCH"])
def update_user(user_id):
    user = Users.query.get(user_id)
    user_phone = request.json['user_phone']
    user_email = request.json['user_email']

    if user is None:
        abort(404)
    else:
        user.user_phone = user_phone
        user.user_email = user_email
        db.session.add(user)
        db.session.commit()
        return jsonify({"success": True, "response": "user Details updated"})




if __name__ == '__main__':
  app.run(debug=True)
